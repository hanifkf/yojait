<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboradadminController extends Controller
{
    public function admin (Request $request){
        $myAdmin = $request->session()->get('admin', false);
        if($myAdmin){
            return view('admin.dashboard');
        }else{
            return redirect()->route('indexadmin');
        }
    }
}
